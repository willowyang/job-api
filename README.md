# Simple Job API project #

This repository contains an express project for handling simple job applications and tradie assignments.

### Technologies ###

Technologies have been chosen to complete this project as listed below with reason why they are chosen.

* Node.js

Node JS is a javascript runtime that supports asynchronous I/O processing which makes it the perfect solution for building REST API. The event-driven architecture makes it easy to handle events in the program although it is not necessary to use the event handler in this project. Node application can handle a large number of socket connections due to the asynchronous I/O architecture.

* Express.js

Express is a light-weight web application framework that runs on node. Using Express to build a high performance MVC architecture API is easy and fast. Express Offers a wide range of middleware supports which makes rapid development possible. Express is very reliable and supported by a large open source community.

* diskdb

A light-weight db solution which doesn't depend on an external application to run. It is a good option to build a small demo project. However, it is not recommended to be used in a production environment  due to performance issue and lack of redundancy supports. MongoDB can be a good choice instead. This project can be easily modified to run with a MongoDB databases.

* express-validator

Express-validator offers express.js middleware which makes the input validation easy and fun. It is a good option to build a validation layer in the application.

* morgan

Morgan is logger middleware which can be easily used as a middleware in express.js to log incoming request of the API. The log that it generated can be useful for troubleshooting purpose in the production environment.

### Prerequirement ###

This project required to have node.js version 6.9.1 or higher to be installed. Node.js can be download from this link [https://nodejs.org/en/download/releases/](https://nodejs.org/en/download/releases/).

### Installation ###

Unzip the app to a work directory and run the following command to install development dependencies.

```shell
npm i
```

To install production only dependencies, run the following command instead.

```shell
npm i --production
```

### Start the app ###

Simply run the following command to start the app

```shell
npm start
```

### Silence mode ###

By default, each request to the REST API is logged on the console. To turn off request logging, start the app by passing a silence argument

```shell
npm start silence
```

or

```shell
npm run-script silence
```

### Unit Testing ###

Running the following command allows you to unit test the application.

```shell
npm start silence
```

Please note that the above command only works on Linux or MAC. For testing in a Windows enviroment, please use the following command instead.

```shell
mocha --require co-mocha 'specs/*.spec.js'
```

## Contact

[Willow Yang](https://www.yangsfamily.org)